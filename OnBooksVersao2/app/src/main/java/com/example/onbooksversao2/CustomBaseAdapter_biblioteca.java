package com.example.onbooksversao2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.onbooksversao2.R;

//leva as informações das listas criadas nas classes das telas para as listviews
public class CustomBaseAdapter_biblioteca extends BaseAdapter {

    Context context;
    String listbiblio[];
    String listend[];
    int listImages[];
    LayoutInflater inflater;

    public CustomBaseAdapter_biblioteca(Context ctx, String [] bibliolist, int [] images, String [] endlist){
        this.context = ctx;
        this.listbiblio = bibliolist;
        this.listImages = images;
        this.listend = endlist;
        inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return listbiblio.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.activity_custom_list_view_biblioteca, null);
        TextView txtView = (TextView) convertView.findViewById(R.id.textViewBiblio);
        TextView txtView1 = (TextView) convertView.findViewById(R.id.textViewEnd);
        ImageView fruitImg = (ImageView) convertView.findViewById(R.id.imageIcon1);
        txtView.setText(listbiblio[position]);
        txtView1.setText(listend[position]);
        fruitImg.setImageResource(listImages[position]);
        return convertView;
    }
}
