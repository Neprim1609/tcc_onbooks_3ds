package com.example.onbooksversao2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
//TELA DE LOGIN

public class MainActivity extends AppCompatActivity {
    Button btnCad, btnLog;
    Switch conectado;

    protected void onStart() {
        super.onStart();
        //Acessar o arquivo gerado no SharedPreferences e recuperar os dados
        SharedPreferences ler = getSharedPreferences("contas", MODE_PRIVATE);
        //Recuperar a situação que está gravada
        //O false será o valor padrão caso não seja possível recuperar o valor gravado
        boolean conectado = ler.getBoolean("conectado", false);

        //Se a escolha foi que é para continuar conectado (true) então avança para a tela principal
        if(conectado){
            startActivity(new Intent(MainActivity.this, Register.class));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnCad = findViewById(R.id.btnCadastrar);
        btnLog = findViewById(R.id.btnLoginL);
        conectado = findViewById(R.id.swContinuar);

        //Evento a ser acionado ao mudar o estado do Switch
        conectado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //O objeto "isChecked" na linha acima indica a situação do Switch
                SharedPreferences.Editor gravar = getSharedPreferences("contas", MODE_PRIVATE).edit();
                gravar.putBoolean("conectado", isChecked); //Indica o valor a ser gravado
                //Como não vamos verificar a gravação e enviar uma mensagem, podemos usar o método apply
                gravar.apply(); //Grava a situação do "conecetado"
            }
        });

        btnCad.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent it = new Intent(MainActivity.this, Register.class);
                startActivity(it);
            }
        });

        btnLog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent it2;
                startActivity(new Intent(MainActivity.this, Home.class));
            }
        });
    }

}
