package com.example.onbooksversao2;

import android.os.Bundle;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

//generos dos livros que direciona para as outras fragments q tem os livros em si
public class LivroFragment extends Fragment {
Button btnAventura, btnEducacional, btnhq, btnInfantil, btnPoema, btnRomance, btnSuspense, btnTerror;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_livros, container, false);

        btnAventura = (Button) view.findViewById(R.id.btnAventura);
        btnEducacional = (Button) view.findViewById(R.id.btnEducacional);
        btnhq = (Button) view.findViewById(R.id.btnhq);
        btnInfantil = (Button) view.findViewById(R.id.btninfantil);
        btnPoema = (Button) view.findViewById(R.id.btnPoema);
        btnRomance = (Button) view.findViewById(R.id.btnRomance);
        btnSuspense = (Button) view.findViewById(R.id.btnSuspense);
        btnTerror = (Button) view.findViewById(R.id.btnTerror);


        btnAventura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AventuraFragment aventura = new AventuraFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, aventura).commit();
            }
        });

        btnEducacional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EducacionalFragment educacional = new EducacionalFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, educacional).commit();
            }
        });

        btnhq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HqFragment hq = new HqFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, hq).commit();
            }
        });

        btnInfantil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InfantilFragment infantil = new InfantilFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, infantil).commit();
            }
        });

        btnPoema.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PoemaFragment poema = new PoemaFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, poema).commit();
            }
        });

        btnRomance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RomanceFragment romance = new RomanceFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, romance).commit();
            }
        });

        btnSuspense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SuspenseFragment suspense = new SuspenseFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, suspense).commit();
            }
        });

        btnTerror.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TerrorFragment terror = new TerrorFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, terror).commit();
            }
        });


        setupOnBackPressed();
        return view;
        }

    //função do botão voltar do próprio aparelho
    public void setupOnBackPressed() {

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

                HomeFragment home = new HomeFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, home).commit();

            }
        });
    }}