package com.example.onbooksversao2;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.onbooksversao2.CustomBaseAdapter;
import com.example.onbooksversao2.R;

public class HomeFragment extends Fragment {
    //mudar para o webservice
    //Lista do nome dos titulos
    String fruitlist[] = {"Em outra vida, talvez?", "Os dois morrem no final", "Tudo o que eu sei sobre o amor", "Radio Silêncio", "O jardim das borboletas"};
    //Lista do nome dos autores
    String autorlist[] = {"Taylor Jenkins Reid", "Adam Silvera", "Dolly Alderton", "Alice Oseman", "Dot Hutchison"};
    //Lista das imagens dos livros
    int fruitImages[] = {R.drawable.livrohome1, R.drawable.livrohome2, R.drawable.livrohome6, R.drawable.livrohome4, R.drawable.livrohome5};

    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        listView = (ListView) view.findViewById(R.id.customListView);
        CustomBaseAdapter customBaseAdapter = new CustomBaseAdapter(requireContext().getApplicationContext(), fruitlist, fruitImages, autorlist);
        listView.setAdapter(customBaseAdapter);

        return view;
    }

    //função do botão voltar do próprio aparelho
    public void setupOnBackPressed() {

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                LivroFragment livro = new LivroFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, livro).commit();


            }
        });
    }
}