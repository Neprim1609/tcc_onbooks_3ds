package com.example.onbooksversao2;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.onbooksversao2.CustomBaseAdapter_biblioteca;
import com.example.onbooksversao2.R;

//dados sobre as bibliotecas neste fragment
public class BibliotecasFragment extends Fragment {
    //mudar para o webservice
    //Lista do nome das bibliotecas
    String biblioList[] = {"Biblioteca - Escola SENAI Prof. Dr. Euryclides de Jesus Zerbini", "Biblioteca - Escola SESI Campinas I - Unidade: Amoreiras"};
    //Lista dos endereços
    String endList[] = {"Av. da Saudade, 125 - Ponte Preta - Campinas/SP Telefone: (19) 3731-2840", "Av. das Amoreiras, 450 - Pq. Itália - Campinas/SP Telefone: (19) 3772-4100"};
    //Lista das bibliotecas (locais)
    int fruitImages[] = {R.drawable.bibliozerbini, R.drawable.biblioamoreira};

    ListView listViewBiblio;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bibliotecas, container, false);

        listViewBiblio = (ListView) view.findViewById(R.id.listViewBiblio);
        CustomBaseAdapter_biblioteca customBaseAdapter = new CustomBaseAdapter_biblioteca(requireContext().getApplicationContext(), biblioList, fruitImages, endList);
        listViewBiblio.setAdapter(customBaseAdapter);

        setupOnBackPressed();

        return view;
    }
    //função do botão voltar do próprio aparelho
    public void setupOnBackPressed() {

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

                HomeFragment home = new HomeFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, home).commit();

            }
        });
    }}
