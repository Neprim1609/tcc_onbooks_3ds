package com.example.onbooksversao2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.onbooksversao2.R;

//leva as informações das listas criadas nas classes das telas para as listviews
public class CustomBaseAdapter extends BaseAdapter {

    Context context;
    String listfruit[];
    String listautor[];
    int listImages[];
    LayoutInflater inflater;

    public CustomBaseAdapter(Context ctx, String [] fruitlist, int [] images, String [] autorlist){
        this.context = ctx;
        this.listfruit = fruitlist;
        this.listImages = images;
        this.listautor = autorlist;
        inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return listfruit.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.activity_custom_list_view, null);
        TextView txtView = (TextView) convertView.findViewById(R.id.txtview);
        TextView txtView1 = (TextView) convertView.findViewById(R.id.txtview1);
        ImageView fruitImg = (ImageView) convertView.findViewById(R.id.imageIcon);
        txtView.setText(listfruit[position]);
        txtView1.setText(listautor[position]);
        fruitImg.setImageResource(listImages[position]);
        return convertView;
    }
}
