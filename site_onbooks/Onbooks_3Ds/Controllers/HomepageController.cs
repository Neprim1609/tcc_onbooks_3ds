﻿using Microsoft.AspNetCore.Mvc;
using Onbooks_3Ds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onbooks_3Ds.Controllers
{
    public class HomepageController : Controller
    {

        public IActionResult Biblioteca()
        {
            return View();
        }
        public IActionResult Delivery()
        {
            return View();
        }
        public IActionResult Generos()
        {
            return View();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login_Cadastro()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login_Cadastro(string nome_usuario, string senha, string email, string telefone, string cpf)
        {/* fazer logica que é a seguinte se os itens pedidos pelo cadastro derem todos offs ele cadastra porem se não ele da login*/
            Cadastrar_Usuarios c = new Cadastrar_Usuarios(nome_usuario, cpf, email, telefone, senha);
            TempData["msg"] = c.Salvar_Usuario();
            return RedirectToAction("Index");
        }




        public IActionResult Produto()
        {
            return View();
        }
    }
}
