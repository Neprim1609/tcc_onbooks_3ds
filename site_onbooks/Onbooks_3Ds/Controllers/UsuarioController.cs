﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Onbooks_3Ds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onbooks_3Ds.Controllers
{
    public class UsuarioController : Controller
    {
        public IActionResult Logar()
        {
            Usuario u = new Usuario("Paulo", "abacaxi");
            //guardo na sessão
            HttpContext.Session.SetString("user",
               JsonConvert.SerializeObject(u));
            /*    //Guardar em um cookie
                Response.Cookies.Append("Abacaxi",
                   JsonConvert.SerializeObject(u));
            */
            TempData["json"] = JsonConvert.SerializeObject(u);
            return RedirectToAction("Listar", "Pessoa");
        }

        public IActionResult Sair()
        {
            Usuario u = JsonConvert.DeserializeObject<Usuario>(HttpContext.Session.GetString("user"));
            HttpContext.Session.Remove("user");
            Response.Cookies.Delete("Abacaxi");
            return RedirectToAction("Listar", "Pessoa");
        }
    }
}
